# TASK-MANAGER

## DEVELOPER

**NAME**: Vadim Belkin

**E-MAIL**: vbelkin@tsconsulting.ru

**E-MAIL**: belkinvo95@gmail.com

## SOFTWARE

**Java**: Jdk 1.8

**OS**: Windows 10

## Hardware

**CPU**: i5

**RAM**: 8.GB

## APPLICATION RUN

```
java -jar ./task-manager.jar
```
## APPLICATION BUILD
```
mvn clean install
```



