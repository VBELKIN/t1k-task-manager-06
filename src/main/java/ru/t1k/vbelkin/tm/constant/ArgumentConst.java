package ru.t1k.vbelkin.tm.constant;

public final class ArgumentConst {
    public final static String VERSION = "-v";
    public final static String HELP = "-h";
    public final static String ABOUT = "-a";
}
